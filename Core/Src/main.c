/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
//#include "app_bluenrg_ms.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "LL_stm32_hd44780.h"
#include <stdio.h>
#define SHT40_ADDRESS (0x44 << 1)
#define SGP30_ADDRESS (0x58 << 1)
//-----------------------------------
#define    capteur_CO2     1

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

/* USER CODE BEGIN PV */
#if (capteur_CO2==0)
	static void SHT40_measure();
	void floatToString(float value, char *buffer, int decimalPlaces);
#else
	static void SGP30_measure();
    void intToString(uint16_t value, char *buffer);
#endif
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
char msg[40];
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  //HAL_I2C_Init(&hi2c1);
  _HD44780_Init(16, 2);
  LCD_On();
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  //MX_BlueNRG_MS_Init();
  /* USER CODE BEGIN 2 */
  _HD44780_Puts(0, 0, "Init");
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  sprintf(msg,"%s","\r\n entree while\r\n");
  while (1)
  {
#if (capteur_CO2==0)

	  SHT40_measure();
#else
	   SGP30_measure();

	  HAL_Delay(500);
#endif



    /* USER CODE END WHILE */

  //MX_BlueNRG_MS_Process();
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 2;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1|GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PA1 PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_1|GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
#if (capteur_CO2==0)
static void SHT40_measure(){
	char tab[10];
	HAL_StatusTypeDef ret;
	uint8_t data_tx[1] = {0xFD};
	uint8_t data_rx[6];
	ret = HAL_I2C_Master_Transmit(&hi2c1, SHT40_ADDRESS, &data_tx, 1, 1);
	if ( ret != HAL_OK ) {
	  printf("Error Tx \r\n");

	}
	else{
		//read bytes
		 HAL_Delay(10);
		 ret =  HAL_I2C_Master_Receive(&hi2c1, SHT40_ADDRESS, (uint8_t*)&data_rx, 6,1000);
		 if ( ret != HAL_OK ) {
			 printf("Error Rx\r\n");
		 }
		 else{
			 for(int i = 0; i < 6 ; i++){
				 printf("data_rx[%i] = %u \n",i,data_rx[i]);
			 }
			 float t_ticks = data_rx[0] * 256 + data_rx[1];
			 float rh_ticks = data_rx[3] * 256 + data_rx[4];

			 float t_degC = -45 + 175 * t_ticks/65535;
			 float rh_pRH = -6 + 125 * rh_ticks/65535;

			 printf("t_degC = %.2d \n",t_degC);
			 floatToString(t_degC, tab, 1);
			 _HD44780_Puts(0, 0, "Temp : ");
			 _HD44780_Puts(8, 0, tab);
			 _HD44780_Puts(14, 0, "C");


			 printf("rh_pRH = %.2d \n",rh_pRH);
			 floatToString(rh_pRH, tab, 1);
			 _HD44780_Puts(0, 1, "Humid : ");
			 _HD44780_Puts(9, 1, tab);
			 _HD44780_Puts(14, 1, "%");




		 }
	}
}


void floatToString(float value, char *buffer, int decimalPlaces) {
    int intValue = (int)value;
    int fractionalPart = (int)((value - intValue) * pow(10, decimalPlaces));

    // Convert the integer part to string
    sprintf(buffer, "%d.%0*d", intValue, decimalPlaces, fractionalPart);
}
#else
static void SGP30_measure() {
    char tab[10];
    HAL_StatusTypeDef ret;
    uint8_t data_tx_init[2] = {0x20, 0x03}; // Commande pour l'initialisation
    uint8_t data_tx_measure[2] = {0x20, 0x08}; // Commande pour lire les valeurs CO2 et TVOC
    uint8_t data_rx[6];

    // Initialisation du capteur
    ret = HAL_I2C_Master_Transmit(&hi2c1, SGP30_ADDRESS, data_tx_init, 2, 1);

    if (ret != HAL_OK) {
        printf("Error Init Tx \r\n");
    } else {
        HAL_Delay(10); // Attente pour l'initialisation
    }

    while (1) {
        // Mesure des valeurs
        ret = HAL_I2C_Master_Transmit(&hi2c1, SGP30_ADDRESS, data_tx_measure, 2, 1);

        if (ret != HAL_OK) {
            printf("Error Measure Tx \r\n");
        } else {
            HAL_Delay(30); // Attente pour que les mesures soient disponibles

            ret = HAL_I2C_Master_Receive(&hi2c1, SGP30_ADDRESS, data_rx, 6, 1000);

            if (ret != HAL_OK) {
                printf("Error Rx\r\n");
            } else {
                for (int i = 0; i < 6; i++) {
                    printf("data_rx[%i] = %u \n", i, data_rx[i]);
                }

                uint16_t co2eq = (data_rx[0] << 8) | data_rx[1];
                uint16_t tvoc = (data_rx[3] << 8) | data_rx[4];

               printf("cO2 = %u ppm \n", co2eq);

               intToString(co2eq, tab);

                _HD44780_Puts(0, 0, "cO2eq : ");
                _HD44780_Puts(8, 0, tab);
                _HD44780_Puts(12, 0, "ppm");


               	printf("TVOC = %u ppb \n", tvoc);
                intToString(tvoc, tab);
                _HD44780_Puts(0, 1, "TVOC  : ");
                _HD44780_Puts(8, 1, tab);
                _HD44780_Puts(12, 1, "ppb");
            }
        }

        HAL_Delay(2000); // Temporisation de 2 secondes
    }
}

void intToString(uint16_t value, char *buffer) {
    sprintf(buffer, "%u ", value);

}
#endif
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
