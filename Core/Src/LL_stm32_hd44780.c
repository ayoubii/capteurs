/**	
 * |----------------------------------------------------------------------
 * | Copyright (c) 2016 Tilen Majerle
 * |  
 * | modifi� par L LEDUc1 1r septembre 2018 / aout 2023 pour STM32L0
 * |
 * | Permission is hereby granted, free of charge, to any person
 * | obtaining a copy of this software and associated documentation
 * | files (the "Software"), to deal in the Software without restriction,
 * | including without limitation the rights to use, copy, modify, merge,
 * | publish, distribute, sublicense, and/or sell copies of the Software, 
 * | and to permit persons to whom the Software is furnished to do so, 
 * | subject to the following conditions:
 * | 
 * | The above copyright notice and this permission notice shall be
 * | included in all copies or substantial portions of the Software.
 * | 
 * | THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * | EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * | OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * | AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * | HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * | WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 * | FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * | OTHER DEALINGS IN THE SOFTWARE.
 * |----------------------------------------------------------------------
 */
#include "LL_stm32_hd44780.h"

/* Private function */
static uint16_t GPIO_UsedPins[13] = {0,0,0,0,0,0,0,0,0,0,0,0,0};

/* Private HD44780 structure */
typedef struct {
	uint8_t DisplayControl;
	uint8_t DisplayFunction;
	uint8_t DisplayMode;
	uint8_t Rows;
	uint8_t Cols;
	uint8_t currentX;
	uint8_t currentY;
} HD44780_Options_t;

/* Private functions */
static void _HD44780_InitPins(void);
static void _HD44780_Cmd(uint8_t cmd);
static void _HD44780_Cmd4bit(uint8_t cmd);
static void _HD44780_Data(uint8_t data);

// void _HD44780_CursorSet(uint8_t col, uint8_t row);

/* Private variable */
static HD44780_Options_t HD44780_Opts;

/* Pin definitions */
#define HD44780_RS_LOW              _GPIO_SetPinLow(HD44780_RS_PORT, HD44780_RS_PIN)
#define HD44780_RS_HIGH             _GPIO_SetPinHigh(HD44780_RS_PORT, HD44780_RS_PIN)
#define HD44780_E_LOW               _GPIO_SetPinLow(HD44780_E_PORT, HD44780_E_PIN)
#define HD44780_E_HIGH              _GPIO_SetPinHigh(HD44780_E_PORT, HD44780_E_PIN)

#define HD44780_E_BLINK             HD44780_E_HIGH; HD44780_Delay(50); HD44780_E_LOW; HD44780_Delay(50)

#define HD44780_Delay(x)      Delay_us(x)

/* Commands*/
#define HD44780_CLEARDISPLAY        0x01
#define HD44780_RETURNHOME          0x02
#define HD44780_ENTRYMODESET        0x04
#define HD44780_DISPLAYCONTROL      0x08
#define HD44780_CURSORSHIFT         0x10
#define HD44780_FUNCTIONSET         0x20
#define HD44780_SETCGRAMADDR        0x40
#define HD44780_SETDDRAMADDR        0x80

/* Flags for display entry mode */
#define HD44780_ENTRYRIGHT          0x00
#define HD44780_ENTRYLEFT           0x02
#define HD44780_ENTRYSHIFTINCREMENT 0x01
#define HD44780_ENTRYSHIFTDECREMENT 0x00

/* Flags for display on/off control */
#define HD44780_DISPLAYON           0x04
#define HD44780_CURSORON            0x02
#define HD44780_BLINKON             0x01

/* Flags for display/cursor shift */
#define HD44780_DISPLAYMOVE         0x08
#define HD44780_CURSORMOVE          0x00
#define HD44780_MOVERIGHT           0x04
#define HD44780_MOVELEFT            0x00

/* Flags for function set */
#define HD44780_8BITMODE            0x10
#define HD44780_4BITMODE            0x00
#define HD44780_2LINE               0x08
#define HD44780_1LINE               0x00
#define HD44780_5x10DOTS            0x04
#define HD44780_5x8DOTS             0x00

/*fonction Delay_us()
* micro : nombre de microseconde d'attente
*/
void Delay_us(uint16_t micro)
{ 	uint16_t mult = HAL_RCC_GetHCLKFreq()/1000000/8 ;  //MAJ 2023 pour horloge inférieure à 8MHz
   if (mult==0) mult = 1;
	micro = micro * mult;
	while (micro)
	{
		asm ("nop"); // ou __ASM volatile ("NOP");
		micro--;
	}
}
//GPIO
/* Private functions */
void _GPIO_INT_EnableClock(GPIO_TypeDef* GPIOx);
void _GPIO_INT_DisableClock(GPIO_TypeDef* GPIOx);
void _GPIO_INT_Init(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, _GPIO_Mode_t GPIO_Mode, _GPIO_OType_t GPIO_OType, _GPIO_PuPd_t GPIO_PuPd, _GPIO_Speed_t GPIO_Speed);

void _GPIO_Init(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, _GPIO_Mode_t GPIO_Mode, _GPIO_OType_t GPIO_OType, _GPIO_PuPd_t GPIO_PuPd, _GPIO_Speed_t GPIO_Speed) {
	/* Check input */
	if (GPIO_Pin == 0x00) {
		return;
	}
	
	/* Enable clock for GPIO */
	_GPIO_INT_EnableClock(GPIOx);
	__HAL_RCC_GPIOB_CLK_ENABLE(); //pour broche CLCD en PB9 , LL for STM32L010
	
	/* Do initialization */
	_GPIO_INT_Init(GPIOx, GPIO_Pin, GPIO_Mode, GPIO_OType, GPIO_PuPd, GPIO_Speed);
}

uint16_t _GPIO_GetPortSource(GPIO_TypeDef* GPIOx) {
	/* Get port source number */
	/* Offset from GPIOA                       Difference between 2 GPIO addresses */
	return ((uint32_t)GPIOx - (GPIOA_BASE)) / ((GPIOB_BASE) - (GPIOA_BASE));
}

/* Private functions */
void _GPIO_INT_EnableClock(GPIO_TypeDef* GPIOx) {
	/* Set bit according to the 1 << portsourcenumber */
	#if defined(STM32F0xx)
	   RCC->AHBENR |= (1 << (_GPIO_GetPortSource(GPIOx) + 17));
	#else
       #if defined(STM32L010xB)  // cas de la maquette nucléo STM32L010RB ; défini dans projet/properties/#Symbols   MAJ2023
	     RCC->AHBENR |= (1 << _GPIO_GetPortSource(GPIOx));   //MAJ 2023
       #else
	    RCC->AHB1ENR |= (1 << _GPIO_GetPortSource(GPIOx)); //cas F446
	   #endif
    #endif
}

void _GPIO_INT_DisableClock(GPIO_TypeDef* GPIOx) {
	/* Clear bit according to the 1 << portsourcenumber */
	#if defined(STM32F0xx)
	RCC->AHBENR &= ~(1 << (_GPIO_GetPortSource(GPIOx) + 17));
	#else
     #if defined(STM32L010xB)  // cas de la maquette nucléo STM32L010RB ; défini dans projet/properties/#Symbols   MAJ2023
	   RCC->AHBENR &= ~(1 << _GPIO_GetPortSource(GPIOx)); //MAJ 2023
      #else
	    RCC->AHB1ENR &= ~ (1 << _GPIO_GetPortSource(GPIOx));  // cas F446
	   #endif


	#endif
}

void _GPIO_INT_Init(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, _GPIO_Mode_t GPIO_Mode, _GPIO_OType_t GPIO_OType, _GPIO_PuPd_t GPIO_PuPd, _GPIO_Speed_t GPIO_Speed) {
	uint8_t pinpos;
	uint8_t ptr = _GPIO_GetPortSource(GPIOx);
	
	#if defined(STM32F0xx)
	/* STM32F0xx series does not have FAST speed mode available */
	if (GPIO_Speed == _GPIO_Speed_Fast) {
		/* Set speed to high mode */
		GPIO_Speed = _GPIO_Speed_High;
	}
	#endif
	
	/* Go through all pins */
	for (pinpos = 0; pinpos < 0x10; pinpos++) {
		/* Check if pin available */
		if ((GPIO_Pin & (1 << pinpos)) == 0) {
			continue;
		}
		
		/* Pin is used */
		GPIO_UsedPins[ptr] |= 1 << pinpos;
		
		/* Set GPIO PUPD register */
		GPIOx->PUPDR = (GPIOx->PUPDR & ~(0x03 << (2 * pinpos))) | ((uint32_t)(GPIO_PuPd << (2 * pinpos)));
		
		/* Set GPIO MODE register */
		GPIOx->MODER = (GPIOx->MODER & ~((uint32_t)(0x03 << (2 * pinpos)))) | ((uint32_t)(GPIO_Mode << (2 * pinpos)));
		
		/* Set only if output or alternate functions */
		if (GPIO_Mode == _GPIO_Mode_OUT || GPIO_Mode == _GPIO_Mode_AF) {
			/* Set GPIO OTYPE register */
			GPIOx->OTYPER = (GPIOx->OTYPER & ~(uint16_t)(0x01 << pinpos)) | ((uint16_t)(GPIO_OType << pinpos));
			
			/* Set GPIO OSPEED register */
			GPIOx->OSPEEDR = (GPIOx->OSPEEDR & ~((uint32_t)(0x03 << (2 * pinpos)))) | ((uint32_t)(GPIO_Speed << (2 * pinpos)));
		}
	}
}




void _HD44780_Init(uint8_t cols, uint8_t rows) {
	/* Initialize delay */
	//TM_DELAY_Init();
	
	/* Init pinout */
	_HD44780_InitPins();
	
	LCD_On(); // LCD ON
	
	/* At least 40ms */
	HD44780_Delay(45000);
	
	/* Set LCD width and height */
	HD44780_Opts.Rows = rows;
	HD44780_Opts.Cols = cols;
	
	/* Set cursor pointer to beginning for LCD */
	HD44780_Opts.currentX = 0;
	HD44780_Opts.currentY = 0;
	
	HD44780_Opts.DisplayFunction = HD44780_4BITMODE | HD44780_5x8DOTS | HD44780_1LINE;
	if (rows > 1) {
		HD44780_Opts.DisplayFunction |= HD44780_2LINE;
	}
	
	/* Try to set 4bit mode */
	_HD44780_Cmd4bit(0x03);
	HD44780_Delay(4500);
	
	/* Second try */
	_HD44780_Cmd4bit(0x03);
	HD44780_Delay(4500);
	
	/* Third goo! */
	_HD44780_Cmd4bit(0x03);
	HD44780_Delay(4500);	
	
	/* Set 4-bit interface */
	_HD44780_Cmd4bit(0x02);
	HD44780_Delay(100);
	
	/* Set # lines, font size, etc. */
	_HD44780_Cmd(HD44780_FUNCTIONSET | HD44780_Opts.DisplayFunction);

	/* Turn the display on with no cursor or blinking default */
	HD44780_Opts.DisplayControl = HD44780_DISPLAYON;
	_HD44780_DisplayOn();

	/* Clear lcd */
	_HD44780_Clear();

	/* Default font directions */
	HD44780_Opts.DisplayMode = HD44780_ENTRYLEFT | HD44780_ENTRYSHIFTDECREMENT;
	_HD44780_Cmd(HD44780_ENTRYMODESET | HD44780_Opts.DisplayMode);

	/* Delay */
	HD44780_Delay(4500);
}

void _HD44780_CursorSet(uint8_t col, uint8_t row) {
	uint8_t row_offsets[] = {0x00, 0x40, 0x14, 0x54};

	/* Go to beginning */
	if (row >= HD44780_Opts.Rows) {
		row = 0;
	}

	/* Set current column and row */
	HD44780_Opts.currentX = col;
	HD44780_Opts.currentY = row;

	/* Set location address */
	_HD44780_Cmd(HD44780_SETDDRAMADDR | (col + row_offsets[row]));
}

void _HD44780_Clear(void) {
	_HD44780_Cmd(HD44780_CLEARDISPLAY);
	HD44780_Delay(3000);
}

void _HD44780_Puts(uint8_t x, uint8_t y, char* str) {
	_HD44780_CursorSet(x, y);
	while (*str) {
		if (HD44780_Opts.currentX >= HD44780_Opts.Cols) {
			HD44780_Opts.currentX = 0;
			HD44780_Opts.currentY++;
			_HD44780_CursorSet(HD44780_Opts.currentX, HD44780_Opts.currentY);
		}
		if (*str == '\n') {
			HD44780_Opts.currentY++;
			_HD44780_CursorSet(HD44780_Opts.currentX, HD44780_Opts.currentY);
		} else if (*str == '\r') {
			_HD44780_CursorSet(0, HD44780_Opts.currentY);
		} else {
			_HD44780_Data(*str);
			HD44780_Opts.currentX++;
		}
		str++;
	}
}

void _HD44780_DisplayOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_DISPLAYON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void _HD44780_DisplayOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_DISPLAYON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void _HD44780_BlinkOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_BLINKON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void _HD44780_BlinkOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_BLINKON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void _HD44780_CursorOn(void) {
	HD44780_Opts.DisplayControl |= HD44780_CURSORON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void TM_HD44780_CursorOff(void) {
	HD44780_Opts.DisplayControl &= ~HD44780_CURSORON;
	_HD44780_Cmd(HD44780_DISPLAYCONTROL | HD44780_Opts.DisplayControl);
}

void _HD44780_ScrollLeft(void) {
	_HD44780_Cmd(HD44780_CURSORSHIFT | HD44780_DISPLAYMOVE | HD44780_MOVELEFT);
}

void _HD44780_ScrollRight(void) {
	_HD44780_Cmd(HD44780_CURSORSHIFT | HD44780_DISPLAYMOVE | HD44780_MOVERIGHT);
}

void _HD44780_CreateChar(uint8_t location, uint8_t *data) {
	uint8_t i;
	/* We have 8 locations available for custom characters */
	location &= 0x07;
	_HD44780_Cmd(HD44780_SETCGRAMADDR | (location << 3));
	
	for (i = 0; i < 8; i++) {
		_HD44780_Data(data[i]);
	}
}

void _HD44780_PutCustom(uint8_t x, uint8_t y, uint8_t location) {
	_HD44780_CursorSet(x, y);
	_HD44780_Data(location);
}

/* Private functions */
static void _HD44780_Cmd(uint8_t cmd) {
	/* Command mode */
	HD44780_RS_LOW;
	
	/* High nibble */
	_HD44780_Cmd4bit(cmd >> 4);
	/* Low nibble */
	_HD44780_Cmd4bit(cmd & 0x0F);
}

static void _HD44780_Data(uint8_t data) {
	/* Data mode */
	HD44780_RS_HIGH;
	
	/* High nibble */
	_HD44780_Cmd4bit(data >> 4);
	/* Low nibble */
	_HD44780_Cmd4bit(data & 0x0F);
}

static void _HD44780_Cmd4bit(uint8_t cmd) {
	/* Set output port */
	_GPIO_SetPinValue(HD44780_D7_PORT, HD44780_D7_PIN, (cmd & 0x08));
	_GPIO_SetPinValue(HD44780_D6_PORT, HD44780_D6_PIN, (cmd & 0x04));
	_GPIO_SetPinValue(HD44780_D5_PORT, HD44780_D5_PIN, (cmd & 0x02));
	_GPIO_SetPinValue(HD44780_D4_PORT, HD44780_D4_PIN, (cmd & 0x01));
	HD44780_E_BLINK;
}



static void _HD44780_InitPins(void) {
	/* Init all pins */
	_GPIO_Init(HD44780_RS_PORT, HD44780_RS_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_E_PORT, HD44780_E_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_D4_PORT, HD44780_D4_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_D5_PORT, HD44780_D5_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_D6_PORT, HD44780_D6_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_D7_PORT, HD44780_D7_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	_GPIO_Init(HD44780_CLCD_PORT, HD44780_CLCD_PIN, _GPIO_Mode_OUT, _GPIO_OType_PP, _GPIO_PuPd_NOPULL, _GPIO_Speed_Low);
	
	/* Set pins low */
	_GPIO_SetPinLow(HD44780_RS_PORT, HD44780_RS_PIN);
	_GPIO_SetPinLow(HD44780_E_PORT, HD44780_E_PIN);
	_GPIO_SetPinLow(HD44780_D4_PORT, HD44780_D4_PIN);
	_GPIO_SetPinLow(HD44780_D5_PORT, HD44780_D5_PIN);
	_GPIO_SetPinLow(HD44780_D6_PORT, HD44780_D6_PIN);
	_GPIO_SetPinLow(HD44780_D7_PORT, HD44780_D7_PIN);
	_GPIO_SetPinLow(HD44780_CLCD_PORT, HD44780_CLCD_PIN);
}

// pour etre conforme à printf
void _HD44780_PutChar(uint8_t c)
{   if (c != 0)
  {
	if (c == '\n')

	HD44780_Opts.currentY++;
	else
	_HD44780_Data(c);
	}
}

void LCD_On()  //LL
{
	_GPIO_SetPinHigh(HD44780_CLCD_PORT, HD44780_CLCD_PIN);
	/* Delay */
	HD44780_Delay(10);
}

void LCD_Off()  //LL
{
	_GPIO_SetPinLow(HD44780_CLCD_PORT, HD44780_CLCD_PIN);
}
